require 'gitlab'
require 'csv'
require 'optparse'

DEFAULT_API_URL = 'https://gitlab.com/api/v4'
DEFAULT_PROJECT_PATH = 'gitlab-org/gitlab'
DEFAULT_FULL_SET_SIZE = 2000
DEFAULT_TRAINING_SPLIT = 0.7
DEFAULT_EVALUATION_SPLIT = 0.2
DEFAULT_TEST_SPLIT = 0.1
DEFAULT_CSV_FILE_PREFIX = 'issue'

FEATURE_LIST = %w(
  title
  description
  label_list
  author_id
)

options = {
  endpoint: DEFAULT_API_URL,
  project: DEFAULT_PROJECT_PATH,
  full_set_size: DEFAULT_FULL_SET_SIZE,
  training_split: DEFAULT_TRAINING_SPLIT,
  evaluation_split: DEFAULT_EVALUATION_SPLIT,
  test_split: DEFAULT_TEST_SPLIT,
  csv_file_prefix: DEFAULT_CSV_FILE_PREFIX,
}

OptionParser.new do |opts|
  opts.banner = "Usage: establish_issue_data_sets.rb [options]"

  opts.on("--token=TOKEN", "Access token for GitLab API") do |v|
    options[:token] = v
  end

  opts.on("-h", "--help", "Prints this help") do
    puts opts
    exit
  end
end.parse!

raise ArgumentError('Token must be provided') unless options[:token]

Gitlab.configure do |config|
  config.endpoint       = options[:endpoint]
  config.private_token  = options[:token]
end

raw_issues = []
per_page = 100
page = 0
while raw_issues.flatten.length < options[:full_set_size] do
  page += 1
  puts "Retrieving page #{page}..."
  new_issues = Gitlab.issues(options[:project], { per_page: per_page, page: page })

  #redact confidential issues
  raw_issues << new_issues.select { |issue| issue['confidential'] == false }
end

all_issues = raw_issues.flatten.map { |i| i.to_h }
issues = all_issues.take(options[:full_set_size])

raw_issues = nil

issues.each do |issue|
  issue['label_list'] = issue['labels'].join('_i_')
  issue['author_id'] = issue['author']['id']
end

features_for_issues = issues.each_with_object([]) do |issue, arr|
  arr << {}.tap do |narrow_issue|
    FEATURE_LIST.each do |feature|
      narrow_issue[feature] = issue[feature]
    end
  end
end

clean_filename = options[:csv_file_prefix] + '_clean.csv'

puts "Writing #{clean_filename}"
puts "Set Size of #{features_for_issues.length} items"

CSV.open(clean_filename, "wb") do |csv|
  features_for_issues.each do |issue|
    csv << issue.values
  end
end

# shuffle entire set
features_for_issues.shuffle!

num_training_items = (features_for_issues.length * options[:training_split]).to_i
num_evaluation_items = (features_for_issues.length * options[:evaluation_split]).to_i
num_test_items = (features_for_issues.length * options[:test_split]).to_i

eval_start_index = num_training_items - 1
test_start_index = eval_start_index + num_evaluation_items

# split set according to ratios
training_set = features_for_issues.slice(0, num_training_items)
eval_set = features_for_issues.slice(eval_start_index, num_evaluation_items)
test_set = features_for_issues.slice(test_start_index, num_test_items)

training_filename = options[:csv_file_prefix] + '_training.csv'
evaluation_filename = options[:csv_file_prefix] + '_evaluation.csv'
test_filename = options[:csv_file_prefix] + '_test.csv'

[
  { set: training_set, filename: training_filename },
  { set: eval_set, filename: evaluation_filename },
  { set: test_set, filename: test_filename },
].each do |split|
  CSV.open(split[:filename], "wb") do |csv|
    puts "Writing #{split[:filename]}"
    puts "Set Size of #{split[:set].length} items"

    split[:set].each do |issue|
      csv << issue.values
    end
  end
end


